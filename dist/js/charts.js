(function(exports, document, undefined){
/**
 * Extend an object, by cloning its properties (like jQuery extend)
 * @param out
 * @returns {*|{}}
 */
var extend = function (out) {
  out = out || {};

  for (var i = 1; i < arguments.length; i++) {
    var obj = arguments[i];

    if (!obj)
      continue;

    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (typeof obj[key] === 'object')
          out[key] = extend(out[key], obj[key]);
        else
          out[key] = obj[key];
      }
    }
  }

  return out;
};


/**
 * D3 hack to get a callback after transitioning multiple elements.
 * Use: d3.selectAll("g").transition().call(endall, function() { console.log("done") });
 * @param transition
 * @param callback
 */
function endall(transition, callback) {
  if (!callback) callback = function(){};
  if (transition.size() === 0) { callback() }
  var n = 0;
  transition
    .each(function() { ++n; })
    .each("end", function() { if (!--n) callback.apply(this, arguments); });
}

/**
 * IsDOMElement - Check if variable is DOM Element
 * @param element
 * @returns {true|false}
 */
function isDOMElement(element) {
  return (typeof element === 'object') && (element.nodeType===1) && (typeof element.style === 'object') && (typeof element.ownerDocument === 'object')
}

/**
 * wrapSVGText - Wrap long labels
 * @param  {DOM Element} text
 * @param  {Number} width
 */
function wrapSVGText(element, width) {
  return element.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        x = text.attr("x"),
        dy = Math.abs(parseFloat(text.attr("dy"))),
        tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy",dy);

    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

/**
 * Clone object
 * @param  {Object} obj - Object to clone
 * @return {Object} Object cloned
 */
function cloneObject(obj) {
  if (obj === null || typeof(obj) !== 'object' || 'isActiveClone' in obj)
    return obj;

  if (obj instanceof Date)
    var temp = new obj.constructor(); //or new Date(obj);
  else
    var temp = obj.constructor();

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      obj['isActiveClone'] = null;
      temp[key] = cloneObject(obj[key]);
      delete obj['isActiveClone'];
    }
  }

  return temp;
}

/*
  Get max and min value in array
 */
Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

Array.prototype.min = function() {
  return Math.min.apply(null, this);
};
/*
  Donut chart
*/

var Donut = function(options) {
  var options = options || {};

  if(!this.handleParameters(options).hasOwnProperty('error')) {

    // If all is ok, let's build it
    this.initialize();
  }
}

Donut.prototype = {

  /**
   * mainProps
   * @type {Object}
   */
  mainProps: {
    el: null,
    data: [],
    legendLabel: '',
    commonSublabel: '',
    commonSubunit: '',
    extraData: [],
    classesPrefix: 'ch',
    chartProps: null
  },

  /**
   * fixedChartProps
   * @type {Object}
   */
  fixedChartProps: {
    minPadding: 30,
  },

  /**
   * defaultChartProps - default chart props
   * @type {Object}
   */
  defaultChartProps: {

    // Width of SVG Element (influences on "radius" prop if "radius" prop not defined)
    width: 800,

    // Height of SVG Element (influences on "radius" prop if "radius" prop not defined)
    height: 400,

    // SVG padding
    padding: 30,
    
    // Thickness of donut.
    thickness: 30,

    // Radius of donut. By default, calculated from width and height
    radius: null,
    
    // Line length between section chart and button
    labelLineLength: 65,

    // Limit when chart label should be displayed on bottom
    minValueForStandardLabelDisplay: 10,

    // Margin on right of labels wrapper (used to display the chart units)
    bottomLabelWrapperRightMargin: 100,

    // Number of labels by line on bottom of chart
    bottomLabelsPerWidth: 3,

    // Animation duration
    animationDuration: 3000,

    // Define animation settings
    animationSettings: function(totalDuration) {
      return {
        arcs: {
          delay: 0,
          duration: .4 * totalDuration,
          ease: d3.easeCubicInOut
        },
        labelElement: {
          delay: .3 * totalDuration,
          duration: .2 * totalDuration,
          ease: d3.easeCubicInOut
        },
        labelPath: {
          delay: .4 * totalDuration,
          duration: .2 * totalDuration,
          ease: d3.easeCircleOut
        },
        labelButton: {
          delay: .4 * totalDuration,
          duration: .2 * totalDuration,
          ease: d3.easeCubicInOut
        },
        labelButtonText: {
          delay: .5 * totalDuration,
          duration: .15 * totalDuration,
          ease: d3.easeCubicInOut
        },
        labelText: {
          delay: .6 * totalDuration - (this.hasEmptyData ? - .4: 0),
          duration: .15 * totalDuration,
          ease: d3.easeCubicInOut
        },
        legendLabel: {
          delay: .6 * totalDuration - (this.hasEmptyData ? - .4: 0),
          duration: .15 * totalDuration,
          ease: d3.easeCubicInOut
        }
      };
    },

    /*
      SVG attributes : can be overiden from CSS
      Allows chart elements to be presentable without CSS
     */
    
    // Chart sections SVG attributes
    sectionStyles: {
      'fill': '#CCCCCC',
      'stroke-width': 2,
      'stroke': '#FFFFFF'
    },

    // Line (between label and section) SVG attributes
    labelLineStyles: {
      'stroke-width': 2,
      'stroke': '#CCCCCC',
      'fill': 'none'
    },

    // Button "+" text tag SVG attributes
    labelButtonTextStyles: {
      'fill': '#FFFFFF',
      'text-anchor': 'middle',
      'alignment-baseline': 'central',
      'font-size': '1.5em',
      'dy': -1
    },

    // Button "+" circle tag SVG attributes
    labelButtonCircleStyles: {
      r: 15
    }
  },

  /**
   * Initialize chart
   */
  initialize: function() {
    this.prepare();
    this.build();
  },

  /**
   * Handle parameters
   * @param  {Object} options
   */
  handleParameters: function(options) {

    this.silentMode = options.hasOwnProperty('silent') && typeof options.silent == 'boolean' ? this.silentMode: false; 

    // Check if parameters are not missing
    if(!options.hasOwnProperty('el')) {
      return this.handleError('missing_parameter_element');

    } else if(!options.hasOwnProperty('data')) {
      return this.handleError('missing_parameter_data');
    }

    // Integrate element if findable
    if(isDOMElement(options.el)) {
      this.$el = d3.select(options.el);

    } else if(typeof options.el === 'string') {
      var el = document.querySelector(options.el);
      if(!isDOMElement(el)) {
        return this.handleError('bad_type_parameter_element');
      }
      this.$el = d3.select(el);

    } else {
      return this.handleError('bad_type_parameter_element');
    }

    // Integrate data if well formated
    if(options.data instanceof Array) {
      this.realData = options.data;
    } else {
      return this.handleError('bad_type_parameter_data');
    }

    // Integrate comparison data
    var extraData = options.extraData || this.mainProps.extraData;

    if(extraData instanceof Array && extraData.length == this.realData.length) {
      var compData = extraData;
      for(var i = 0, l = compData.length; i < l; i++) {
        var settableProperties = ['label', 'subLabel', 'subValue', 'color', 'subValue', 'url'];
        for(var _i = 0, _l = settableProperties.length; _i < _l; _i++) {
          if(compData[i].hasOwnProperty(settableProperties[_i])) {
            this.realData[i][settableProperties[_i]] = compData[i][settableProperties[_i]];
          }          
        }
      }
    }

    // Integrate chart properties
    options.chartProps = options.chartProps || {};
    this.props = this.defineProperties(options.chartProps);

    this.legendLabel = options.legendLabel || this.mainProps.legendLabel;
    this.classesPrefix = options.classesPrefix || this.mainProps.classesPrefix;
    this.comparisonTitle = options.commonSublabel || this.mainProps.commonSublabel;
    this.commonSubunit = options.commonSubunit || this.mainProps.commonSubunit;

    return true;
  },

  /**
   * Merge input and default properties
   * @param  {Object} inputProps - Properties given on chart instantiation
   * @return {Object} Computed properties
   */
  defineProperties: function(inputProps) {
    var props = extend(this.defaultChartProps, inputProps);
    
    // Prevent radius be too large
    var maxRadius = Math.min(props.width, props.height) / 2 - 2 * this.fixedChartProps.minPadding;
    props.padding = Math.max(this.fixedChartProps.minPadding, props.padding);
    props.radius = props.radius === null ? maxRadius : Math.min(maxRadius, props.radius);

    return props;
  },

  events: function() {
    var $elements = this.getElementsToHighlight(),
      _this = this;

    for(var i = 0; i < $elements.length; i++) {
      $elements[i].on('mouseenter', function(d) {
        _this.highlightSection(d);
      });
      $elements[i].on('mouseout', function(d) {
        _this.unHighlightSection(d);
      });
    }
  },

  getElementsToHighlight: function() {

    var $elements = [
      this.$chartWrapper.selectAll('g.'+ this.classesPrefix + '-arc'),
      this.$labelElementsWrapper.selectAll('g.'+ this.classesPrefix +'-label-element'),
      this.$labels.selectAll('div.'+ this.classesPrefix + '-label')
    ];

    return $elements;
  },

  /**
   *  gighlightSection - hightlight section (mouseover)
   */
  highlightSection: function(sectionData) {
    
    var $elements = this.getElementsToHighlight();

    for(var i = 0, l = $elements.length; i < l; i++) {
      $elements[i] = $elements[i].filter(function(d) { return d.index == sectionData.index; });
    }

    for(var i = 0, l = $elements.length; i < l; i++) {
      $elements[i]
        .attr('class', function(d) {
          var currentClass = d3.select(this).attr('class');
          return currentClass.indexOf('hightlighted') !== -1 ? currentClass: currentClass + ' hightlighted';
        });
    }
  },

  unHighlightSection: function(sectionData) {
    var $elements = this.getElementsToHighlight();

    for(var i = 0, l = $elements.length; i < l; i++) {
      $elements[i] = $elements[i].filter(function(d) { return d.index == sectionData.index; });
    }

    for(var i = 0, l = $elements.length; i < l; i++) {
      $elements[i]
        .attr('class', function(d) {
          return d3.select(this).attr('class').replace(' hightlighted', '');
        });
    }
  },

  /**
   * Prepare chart building
   * @return {undefined}
   */
  prepare: function() {

    // Prepare chart utils
    this.pieGenerator = d3.pie()
      .sort(null)
      .startAngle(0)
      .value(function(d) {
        return d.value;
      });

    this.arcGenerator = d3.arc()
      .outerRadius(this.props.radius)
      .innerRadius(this.props.radius - this.props.thickness);

    this.labelLineGenerator = d3.line()
      .x(function(d) { return d[0] })
      .y(function(d) { return d[1] });

    this.centerPosition = {
      x: this.props.width * .5,
      y: this.props.height * .5
    };

    // Prepare data
    this.emptyData = this.realData.map(function(d) {
      var o = cloneObject(d);
      o.value = 0;
      return o;
    });

    this.data = this.prepareData(this.emptyData);

    // Create main wrappers
    this.$wrapper = this.$el.append('div')
      .attr('class', this.classesPrefix + '-wrapper')
      .style('width', this.props.width +'px');

    this.$svg = this.$wrapper.append('svg')
      .attr('class', this.classesPrefix + '-donut-chart')
      .attr('width', this.props.width)
      .attr('height', this.props.height);

    this.$chartWrapper = this.$svg.append('g')
        .attr('class', this.classesPrefix + '-inner')
        .attr('transform', 'translate('+ this.centerPosition.x +','+ this.centerPosition.y +')');

    this.$labelElementsWrapper = this.$svg.append('g')
      .attr('class', this.classesPrefix + '-label-elements-wrapper');

    this.$labels = this.$wrapper.append('div')
      .attr('class', this.classesPrefix + '-labels-wrapper');

    this.$legendWrapper = this.$wrapper.append('div')
      .attr('class', this.classesPrefix + '-legend-wrapper')
  },

  updateData: function(data) {
    this.pastData = cloneObject(this.data);
    this.data = this.prepareData(data);
  },

  /**
   * PrepareEmptyData - prepare data when all values == 0
   * @param  {Object} data
   * @return {Object} extended data
   */
  prepareEmptyData: function(data) {
    var _this = this,
      wDivisions = _this.props.bottomLabelsPerWidth + (data.length < (2 * _this.props.bottomLabelsPerWidth) ? - 1: 0),
      hDivisions = Math.floor(data.length / _this.props.bottomLabelsPerWidth) + 1,
      outputData = cloneObject(data);

    outputData.map(function(d, i) {

        d.data = d;

        d.isSmallValue = false;
        d.alignSide = 'left';

        d.labelTextSize = {
          width: _this.props.width / (wDivisions + 1),
          height: _this.props.height / (hDivisions + 1)
        }

        d.labelTextPosition = {
          x: d.labelTextSize.width * .7 + (i % wDivisions) * d.labelTextSize.width,
          y: d.labelTextSize.height + (Math.floor(i / wDivisions)) * d.labelTextSize.height
        };
    });

    return outputData;
  },

  /**
   * PrepareData - Extend data with chart values
   * @param  {Object} data - source data
   * @return {Object} extended data
   */
  prepareData: function(data) {

    var valuesSum = d3.sum(this.realData, function(d) { return d.value; });
    this.hasEmptyData = valuesSum == 0;

    if(this.hasEmptyData) {
      return this.prepareEmptyData(data);
    }

    var _this = this,
      smallValuesCount = 0,
      totalValues = d3.sum(data, function(d) { 
        return d.value; 
      }),
      totalSubvalues = d3.sum(data, function(d) { 
        return d.hasOwnProperty('subValue') ? d.subValue : 0;
      });

    var smallData = [],
      regularData = [];

    // Calculate main values
    data.map(function(d) {

      d.percentage = d.value / totalValues * 100;
      // if(d.hasOwnProperty('subValue') && typeof d.subValue === 'number') {
      //   d.subPercentage = d.subValue / totalSubvalues * 100;
      // }

      d.isSmallValue = d.percentage < _this.props.minValueForStandardLabelDisplay;

      if(d.isSmallValue) {
        smallData.push(d);
        d.smallValueIndex = smallData.length -1;
      } else {
        regularData.push(d);
        d.regularValueIndex = regularData.length -1;
      }
    });

    data = this.pieGenerator(data);

    // Calculate positions, angles etc.
    data.map(function(d, i) {

      d.isSmallValue = d.data.isSmallValue;

      d.middleAngle = regularData.length === 1
        ? 0
        : d.startAngle - Math.PI * .5 + (d.endAngle - d.startAngle) * .5;

      d.horizontalCoef = d.middleAngle >= Math.PI * .5 ? -1: 1,
      d.verticalCoef = d.middleAngle <= 0 || d.middleAngle >= Math.PI ? -1: 1;

      d.sectionMiddlePosition = {
        x: Math.cos(d.middleAngle) * _this.props.radius + _this.centerPosition.x,
        y: Math.sin(d.middleAngle) * _this.props.radius + _this.centerPosition.y
      };

      // Calculate label line positions
      d.lineMiddlePosition = {
        x: d.sectionMiddlePosition.x + _this.props.labelLineLength * .15 * d.horizontalCoef,
        y: d.sectionMiddlePosition.y + _this.props.labelLineLength * .15 * d.verticalCoef
      };
      d.lineEndPosition = {
        x: d.sectionMiddlePosition.x + _this.props.labelLineLength * d.horizontalCoef,
        y: d.sectionMiddlePosition.y + _this.props.labelLineLength * .15 * d.verticalCoef
      };

      // Calculate label group position depending in value < 10
      d.labelGroupPosition = { 
        x: d.isSmallValue
          ? (_this.props.width - _this.props.padding - _this.props.bottomLabelWrapperRightMargin) / _this.props.bottomLabelsPerWidth * (d.data.smallValueIndex % 3) + _this.props.padding * .5
          : d.lineEndPosition.x,

        y: d.isSmallValue
          ? _this.props.height - _this.props.padding * 2.5
          : d.lineEndPosition.y
      };

      d.alignSide = d.horizontalCoef > 0 || d.isSmallValue ? 'left': 'right';

      d.labelTextPosition = {
        x: (d.alignSide === 'left' ? d.labelGroupPosition.x: _this.props.width - d.labelGroupPosition.x)  + _this.props.labelButtonCircleStyles.r * 1.5,
        y: d.labelGroupPosition.y
      }
    });

    return cloneObject(data);
  },

  /**
   * Build chart
   * @return {Object} svg d3 object
   */
  build: function() {

    if(!this.hasEmptyData) {
      this.buildSections();
    }
    this.buildLabels();
    this.buildLegend();
    this.events();
  },

  /**
   * Build chart sections
   */
  buildSections: function() {
    var _this = this;

    this.$arcs = this.$chartWrapper.selectAll('g.'+ this.classesPrefix + '-arc path');

    var arcs = this.$arcs.data(this.data);
    var $newArcs = arcs
      .enter()
        .append('g')
          .attr('class', this.classesPrefix + '-arc');

    $newArcs.append('path')
      .attr('d', this.arcGenerator)
      .attr('class', function(d) {
        return d.data.hasOwnProperty('class') ? d.data.class: _this.props.sectionClass;
      })
      .attr('fill', function(d) {
        return d.data.hasOwnProperty('color') ? d.data.color: _this.props.sectionStyles.fill;
      })
      .each(function(d) {
        for(var styleName in d.data.style) {
          d3.select(this).style(styleName, d.data.style[styleName]);
        }
      });

    arcs
      .exit()
        .remove();

    // Labels elements wrapper
    this.$labelElements = this.$labelElementsWrapper.selectAll('g.'+ this.classesPrefix +'-label-element')
      .data(this.data);

    var $newLabelElements = this.$labelElements.enter()
      .append('g');

    $newLabelElements.merge(this.$labelElements)
      .attr('class', function(d) {
        return _this.classesPrefix +'-label-element' + (d.data.hasOwnProperty('url') && typeof d.data.url === 'string' ? ' is-link': '')
      })
      .style('fill-opacity', 0)
      .style('stroke-opacity', 0);
        
    this.$labelElements
      .exit()
        .remove();

    // Draw label lines
    $newLabelElements
      .append('path')
        .attr('class', this.classesPrefix + '-label-line');

    this.$labelElements
      .selectAll('path.'+ this.classesPrefix + '-label-line')
      .data(function(d) {
        return [d];
      })
      .filter(function(d) {
        return !d.isSmallValue;
      })
      .attr('d', function(d) {
        return _this.labelLineGenerator([
          [ d.sectionMiddlePosition.x, d.sectionMiddlePosition.y ],
          [ d.lineMiddlePosition.x, d.lineMiddlePosition.y ],
          [ d.lineEndPosition.x, d.lineEndPosition.y ]
        ]);
      })
      .each(function(d) {
        for(var styleName in _this.props.labelLineStyles) {
          d3.select(this).attr(styleName, _this.props.labelLineStyles[styleName]);
        }
      })
      .attr('stroke', function(d) {
        return d.data.hasOwnProperty('color') ? d.data.color: _this.props.labelLineStyles.stroke;
      });

    // Button
    this.$labelsButtons = this.$labelElements
      .filter(function(d) {
        return !d.isSmallValue;
      })
      .append('g')
        .attr('class', this.classesPrefix + '-label-button')
        .style('opacity', 0);

    this.$labelsButtons.append('circle')
      .attr('cx', function(d) { return d.labelGroupPosition.x })
      .attr('cy', function(d) { return d.labelGroupPosition.y })
      .attr('fill', function(d) {
        return d.data.hasOwnProperty('color') ? d.data.color: _this.props.sectionStyles.fill;
      })
      .each(function(d) {
        for(var styleName in _this.props.labelButtonCircleStyles) {
          d3.select(this).attr(styleName, _this.props.labelButtonCircleStyles[styleName]);
        }
      });

    this.$labelsButtons.append('text')
      .text('+')
      .attr('x', function(d) { return d.labelGroupPosition.x })
      .attr('y', function(d) { return d.labelGroupPosition.y })
      .each(function(d) {
        for(var styleName in _this.props.labelButtonTextStyles) {
          d3.select(this).attr(styleName, _this.props.labelButtonTextStyles[styleName]);
        }
      })
      .style('fill-opacity', 0);


    return this.$arcs;
  },

  /**
   * Build legend
   */
  buildLegend: function() {

    if(this.hasOwnProperty('legendLabel') && typeof this.legendLabel === 'string') {

      this.$legendWrapper.style('opacity', 0);

      this.$legendWrapper.selectAll('span')
        .data([{ text: this.legendLabel }])
        .enter()
          .append('span')
            .text(function(d) {
              return d.text
            })

      this.$legendWrapper
        .exit()
            .remove();   
    }
  },

  /**
   * Build labels
   */
  buildLabels: function() {
    var _this = this;

    // Label text wrapper
    this.$labelTexts = this.$labels.selectAll('div.'+ _this.classesPrefix + '-label')
      .data(this.data)

    var $newLabelTexts = this.$labelTexts.enter()
        .append('div')
          .attr('class', _this.classesPrefix + '-label');

    this.$labels.selectAll('div.'+ _this.classesPrefix + '-label')
      .attr('class', function(d) {
        return _this.classesPrefix + '-label' + ' ' + 'align-'+ d.alignSide + ' ' + (d.isSmallValue ? ' free-position': 'fixed-position');
      })
      .style('position', function(d) {
        return d.isSmallValue ? 'static': 'absolute';
      })
      .style('top', function(d, i) { return d.labelTextPosition.y + 'px'; })
      .style('text-align', function(d) { return d.alignSide; })
      .style('opacity', 0)
      .each(function(d) {
        d3.select(this).style('left', 0);
        d3.select(this).style('right', 0);

        d3.select(this).style(d.alignSide, function(d) {
          return d.labelTextPosition.x + 'px';
        });
      });

    this.$labelTexts
      .exit()
        .remove();

    var $newLabelTextsA = $newLabelTexts.append('a')
      .attr('href', function(d) {
        return d.data.url ? d.data.url: null;
      });

    // Title
    this.$labelTitles = $newLabelTexts.append('span')
      .attr('class', this.classesPrefix + '-label-title')

      // Update + enter
      this.$labelTitles.merge(this.$labelTexts.select('span.'+ this.classesPrefix +'-label-title'))
          .text(function(d) {
            return d.data.label + (isNaN(d.data.percentage) ? '' : (' ' + ((Math.round(d.data.percentage * 100) / 100)+'').replace('.', ',') + ' %'));
          })
          .style('color', function(d) {
            return d.data.hasOwnProperty('color') ? d.data.color: _this.props.sectionStyles.fill;
          });

    // Subtitle
    this.$labelSubtitle = $newLabelTexts
      .filter(function(d) {
        return !!_this.comparisonTitle || !!d.data.subLabel;
      })
      .append('span')
      .attr('class', this.classesPrefix + '-label-subtitle')

      // Update + enter
      .merge(this.$labelTexts.selectAll('span.'+ this.classesPrefix + '-label-subtitle'))
        .text(function(d) {
          return (!!_this.comparisonTitle ? _this.comparisonTitle : d.data.subLabel) + (d.data.subValue ? ' '+ d.data.subValue +' '+ _this.commonSubunit: '');
        });

    // Vertical centering
    $newLabelTexts.merge(this.$labelTexts)
      .each(function(d) {
        d.labelWidth = this.offsetWidth;
      })
      .style('top', function(d) {
        return (d.labelTextPosition.y - this.offsetHeight * .5) + 'px';
      });

    // Set width labels on empty data "chart"
    if(this.hasEmptyData) {      
      $newLabelTexts.merge(this.$labelTexts)
        .style('width', function(d) {
          return (.8 * d.labelTextSize.width) + 'px';
        });
    }

    $newLabelTextsA
      .style('width', function(d) {
        return _this.hasEmptyData ? (.8 * d.labelTextSize.width) + 'px': (d.labelWidth + _this.props.labelButtonCircleStyles.r) + 'px';
      })
      .style('left', function(d) {
        return _this.hasEmptyData ? '0px': (- _this.props.labelButtonCircleStyles.r * 3) + 'px';
      });
  },

  /**
   * Show
   */
  show: function() {    
    this.updateData(this.realData);
    this.animate();
    this.events();
  },

  /**
   * Hide
   */
  hide: function() {
    var startData = [],
      _this = this;
  },

  /**
   * Animate chart (introduction)
   * @param  {Number} duration 
   */
  animate: function(duration) {
    duration = duration || this.props.animationDuration;

    if(!this.hasEmptyData) {
      this.animateSections(duration);
    }

    this.animateLabels(duration);
  },

  animateSections: function(duration) {
    var _this = this,
      animSettings = this.props.animationSettings(duration);

    // Refresh donut
    this.buildSections();

    this.$chartWrapper.selectAll('g.'+ this.classesPrefix + '-arc path')
      .transition()
      .delay(animSettings.arcs.delay)
      .duration(animSettings.arcs.duration)
      .ease(animSettings.arcs.ease)
        .attrTween('d', function(d, i) {
            var interpolate = d3.interpolate(_this.pastData[i], d);
            return function(t) {
                return _this.arcGenerator(interpolate(t));
            };
        });

    this.$labelElements
      .transition()
      .delay(function(d, i) {
        return animSettings.labelElement.delay + i * 150;
      })
      .duration(animSettings.labelElement.duration)
      .ease(animSettings.labelElement.ease)
        .style('fill-opacity', 1)
        .style('stroke-opacity', 1);

    var $path = this.$labelElements.select('path');
    var totalLength = $path.node().getTotalLength();
    $path
      .attr('stroke-dasharray', totalLength + ' ' + totalLength)
      .attr('stroke-dashoffset', totalLength)
        .transition()
        .delay(function(d, i) {
          return animSettings.labelPath.delay + i * 150;
        })
        .duration(animSettings.labelPath.duration)
        .ease(animSettings.labelPath.ease)
          .attr('stroke-dashoffset', 0);

    this.$labelsButtons
      .transition()
      .delay(function(d, i) {
        return animSettings.labelButton.delay + i * 150;
      })
      .duration(animSettings.labelButton.duration)
      .ease(animSettings.labelButton.ease)
        .style('opacity', 1)
          .on('end', function(d) {
            d3.select(this).style('opacity', null);
            this.style.opacity = '';
          });

    this.$labelsButtons.select('text')
      .transition()
      .delay(function(d, i) {
        return animSettings.labelButtonText.delay + i * 150;
      })
      .duration(animSettings.labelButtonText.duration)
      .ease(animSettings.labelButtonText.ease)
        .style('fill-opacity', 1)
          .on('end', function(d) {
            d3.select(this).style('fill-opacity', null);
            this.style['fill-opacity'] = '';
          })
  },

  animateLabels: function(duration) {
    var _this = this,
      animSettings = this.props.animationSettings(duration);

    // Refresh labels elements
    this.buildLabels();

    this.$labelTexts
      .transition()
      .delay(function(d, i) {
        return animSettings.labelText.delay + i * 150;
      })
      .duration(animSettings.labelText.duration)
      .ease(animSettings.labelText.ease)
        .style('opacity', 1)
          .on('end', function(d) {
            this.style.opacity = '';
          })

    this.buildLegend();

    this.$legendWrapper
      .transition()
        .delay(animSettings.legendLabel.delay)
        .duration(animSettings.legendLabel.duration)
        .ease(animSettings.legendLabel.ease)
          .style('opacity', 1)
            .on('end', function(d) {
              this.style.opacity = '';
            })
  },

  /**
   * Handle chart errors
   * @param  {String} - Error type.
   * @return {Object} Error object
   */
  handleError: function(type) {

    var message = 'Initialization Error';
    var errors = {
      missing_parameter_data: 'Missing "data" parameter.',
      bad_type_parameter_element: 'Bad type "data" parameter. Must be an array.',
      missing_parameter_element: 'Missing DOM "el" selector.',
      bad_type_parameter_element: 'Bad type "el" parameter. Must be a DOM Element or String selector.',
      missing_parameter_comparison: 'Missing property title for comparison'
    };

    for(errorType in errors) {
      if(errorType === type) {
        message = errors[errorType];
      }
    }

    message = 'Donut Chart : ' + message;

    // Print error only if silent mode disabled
    if(this.silentMode) {
      console.error(message);
    }

    return {
      error: message
    };
  }
};

// function Donut(options) {
//   return new DonutClass(options);
// }
function Radar(data, options) {
  // Data
  this.data = data || [];

  // Options
  var o = options || {};
  var defaultOptions = {
    // SIZE - note that you will need to add a padding in CSS to make the labels visible
    // Current default padding in radar css is 80px.
    w: 600,
    h: 600,

    // OFFSET - useful to make the chart more readable by resizing/repositioning elements
    offsetFactor: 1,
    areaOffset: 0.95,
    legendaMaxOffset: 1,
    legendaOffset: 1.2,

    // Other options
    levels: 5, // concentric circles or polygons
    maxValue: 0, // leave to 0, if you want to control max value from data
    radians: 2 * Math.PI, // 360 deg = circle
    colors: [] // array of colors, corresponding to each series of data
  };
  this.options = extend(defaultOptions, o);

  this.initialize();
  this.render();
  // this.animate();
}

Radar.prototype = {

  initialize: function () {

  },

  render: function () {
    // console.log(this.data, this.options);

    var that = this;
    var d = this.data;
    var cfg = this.options;
    var id = this.options.el;

    // Get max value
    cfg.maxValue = Math.max(cfg.maxValue, d3.max(d, function (i) {
      return d3.max(i.map(function (o) {
        return o.value;
      }))
    }));

    // Get axis and label for each axis
    var allAxis = (d[0].map(function (i, j) {
      return i.label;
    }));

    // Useful values
    this.axisCount = allAxis.length;
    this.center = Math.min(cfg.w / 2, cfg.h / 2);
    this.radius = cfg.offsetFactor * Math.min(cfg.w / 2, cfg.h / 2);

    // Add main css classes
    this.$el = d3.select(id)
      .attr("class", "chart radar-chart");

    // Add wrapper, labels wrapper and svg
    this.$wrapper = this.$el
      .append("div")
      .attr("class", "wrapper")
      .style("width", cfg.w + "px")
      .style("height", cfg.h + "px")
      .style("position", "relative");
    this.$labelsWrapper = this.$wrapper
      .append("div")
      .attr("class", "labels-wrapper")
      .style("width", "100%")
      .style("height", "100%")
      .style("position", "absolute");
    this.$svg = this.$wrapper
      .append("svg")
      .attr("class", "svg")
      .attr("width", cfg.w)
      .attr("height", cfg.h);
    var g = this.$svg.append("g");

    // Position helpers
    function getPosition(i, range, offsetFactor, func) {
      offsetFactor = typeof offsetFactor !== 'undefined' ? offsetFactor : 1;
      return range * (1 - offsetFactor * func(i * cfg.radians / that.axisCount));
    }

    function getHorizontalPosition(i, range, offsetFactor) {
      return getPosition(i, range, offsetFactor, Math.sin);
    }

    function getVerticalPosition(i, range, offsetFactor) {
      return getPosition(i, range, offsetFactor, Math.cos);
    }

    // Draw concentric circles or polygons (svg)
    for (var i = 0; i < cfg.levels; i++) {
      var levelRadius = this.radius * ((i + 1) / cfg.levels);
      this.$levels = g
        .append("svg:circle")
        .attr('r', levelRadius)
        .attr("cx", this.center)
        .attr("cy", this.center)
        .style("opacity", 0)
        .attr("class", "level line");

    }

    // Draw axis lines (svg)
    var axis = g.selectAll(".axis")
      .data(d[0])
      .enter()
      .append("line")
      .style("opacity", 0)
      .attr("x1", cfg.w / 2)
      .attr("y1", cfg.h / 2)
      .attr("x2", function (j, i) {
        return getHorizontalPosition(i, cfg.w / 2, cfg.offsetFactor);
      })
      .attr("y2", function (j, i) {
        return getVerticalPosition(i, cfg.h / 2, cfg.offsetFactor);
      })
      .attr("class", "line axis");

    // Draw polygon area (svg)
    d.forEach(function (dataset, idx) {

      // Adapt data to draw the polygon area
      that.startDataValues = [];
      for (var i = 0; i < this.axisCount; i++) {
        that.startDataValues.push([
          getHorizontalPosition(i, cfg.w / 2, cfg.legendaMaxOffset),
          getVerticalPosition(i, cfg.h / 2, cfg.legendaMaxOffset)
        ]);
      }
      that.startDataValues.push(that.startDataValues[0]);

      that.endDataValues = [];
      g.selectAll(".nodes")
        .data(dataset, function (j, i) {
          that.endDataValues.push([
            getHorizontalPosition(i, cfg.w / 2, (parseFloat(Math.max(j.value, 0)) / cfg.maxValue) * cfg.areaOffset),
            getVerticalPosition(i, cfg.h / 2, (parseFloat(Math.max(j.value, 0)) / cfg.maxValue) * cfg.areaOffset)
          ]);
        });
      that.endDataValues.push(that.endDataValues[0]);

      // Draw polygon area
      g.selectAll(".area")
        .data([that.startDataValues])
        .enter()
        .append("polygon")
        .style("stroke", cfg.colors[idx])
        .style("opacity", 0)
        .attr("class", "polygon radar-chart-serie-" + idx)
        .attr("points", function (d) {
          var str = "";
          for (var i = 0; i < d.length; i++) {
            str = str + d[i][0] + "," + d[i][1] + " ";
          }
          return str;
        });

      // Draw legenda (dom)
      this.$labelsWrapper.selectAll(".legenda")
        .data(d[0])
        .enter()
        .append("div")
        .attr("class", "legenda")
        .style("opacity", 0)
        .text(function (d) {
          return d.label;
        })
        .style("color", function (d) {
          return d.color;
        })
        .style("top", function (d, i) {
          var y = getVerticalPosition(i, cfg.h / 2, cfg.legendaOffset);
          return y + "px";
        })
        .style("left", function (d, i) {
          var x = getHorizontalPosition(i, cfg.w / 2, cfg.legendaOffset);
          return x + "px";
        });

      // Draw max value dots (dom)
      this.$labelsWrapper.selectAll(".legenda-dot")
        .data(d[0])
        .enter()
        .append("div")
        .style("opacity", 0)
        .attr("class", "legenda-dot")
        .style("background-color", function (d) {
          return d.color;
        })
        .style("top", function (d, i) {
          var y = getVerticalPosition(i, cfg.h / 2, cfg.legendaMaxOffset);
          return y + "px";
        })
        .style("left", function (d, i) {
          var x = getHorizontalPosition(i, cfg.w / 2, cfg.legendaMaxOffset);
          return x + "px";
        })
        .append("div")
        .style("opacity", 0)
        .attr("class", "value")
        .text(function (d) {
          return d.max;
        });

      // Draw dots with values(dom)
      this.$labelsWrapper.selectAll(".nodes")
        .data(dataset)
        .enter()
        .append("div")
        .style("opacity", 0)
        .style("background-color", cfg.colors[idx])
        .attr("class", "circle radar-chart-serie-" + idx)
        .style("left", function (j, i) {
          var x = getHorizontalPosition(i, cfg.w / 2, (Math.max(j.value, 0) / cfg.maxValue) * cfg.areaOffset);
          return x + "px";
        })
        .style("top", function (j, i) {
          var y = getVerticalPosition(i, cfg.h / 2, (Math.max(j.value, 0) / cfg.maxValue) * cfg.areaOffset);
          return y + "px";
        })
        // .style("transform", "scale(0.5, 0.5)")
        .append("div")
        .attr("class", "value")
        .style("opacity", 0)
        .text(function (j) {
          return Math.max(j.value, 0)
        });

    }.bind(this));
  },

  animate: function () {
    this.animateLevels(
      this.animateAxis.bind(this,
        this.animateValues.bind(this))
    );
  },

  animateLevels: function (callback) {
    var that = this;
    var delay = 100;
    var duration = 800;
    var t = d3.transition()
      .duration(duration)
      .ease(d3.easeCubicInOut);
    var totalDuration = that.options.levels * delay + duration;

    this.$el.selectAll(".level")
      .transition(t)
      .delay(function (d, i) {
        return i * delay;
      })
      .attrTween("r", function (d, i) {
        var levelRadius = that.radius * ((i + 1) / that.options.levels);
        return d3.interpolate(0, levelRadius);
      })
      .style("opacity", 1);

    // Callback
    setTimeout(callback, totalDuration);
  },

  animateAxis: function (callback) {
    var that = this;
    var delay = 100;
    var duration = 500;
    var totalDuration = this.axisCount * delay + duration;

    var t = d3.transition()
      .duration(duration)
      .ease(d3.easeCubicInOut);

    // Lines
    this.$el.selectAll(".axis")
      .transition(t)
      .delay(function (d, i) {
        return i * delay;
      })
      .style("opacity", 1);

    // Max values
    this.$labelsWrapper
      .selectAll(".legenda-dot")
      .transition(t)
      .delay(function (d, i) {
        return i * delay;
      })
      .style("opacity", 1);

    // Legenda
    this.$labelsWrapper
      .selectAll(".legenda")
      .transition(t)
      .delay(function (d, i) {
        return i * (delay * 1.5);
      })
      .style("opacity", 1);

    // Area - start
    this.$svg.selectAll(".polygon")
      .transition(t)
      .delay(function (d, i) {
        return delay * that.axisCount;
      })
      .style("opacity", 0.8);

    // Callback
    setTimeout(callback, totalDuration);
  },

  animateValues: function (callback) {
    var that = this;
    var delay = 100;
    var duration = 1000;
    var totalDuration = duration / 2 + (that.axisCount * delay);

    var tArea = d3.transition()
      .duration(duration)
      .ease(d3.easeCubicInOut);
    var tValues = d3.transition()
      .duration(duration / 4)
      .ease(d3.easeCubicInOut);

    // Area
    this.$svg.selectAll(".polygon")
      .transition(tArea)
      .style("opacity", 1)
      .attr("points", function (d) {
        var endData = that.endDataValues;
        var str = "";
        for (var i = 0; i < d.length; i++) {
          str = str + endData[i][0] + "," + endData[i][1] + " ";
        }
        return str;
      });

    // Values
    setTimeout(function () {
      // Shot circles
      that.$labelsWrapper
        .selectAll(".circle")
        .transition(tValues)
        .delay(function (d, i) {
          return i * (delay);
        })
        .style("opacity", 1);

      that.$labelsWrapper
        .selectAll(".legenda-dot")
        .attr("class", "legenda-dot active");

      // Show labels
      setTimeout(function() {
        that.$labelsWrapper
          .selectAll(".circle")
          .attr("class", "circle active");
      }, totalDuration);

    }, duration);

    // Callback
    setTimeout(callback, totalDuration);
  }

};

/**
 * Charts library.
 * Generates SVG charts in D3, the easy way.
 * Dependencies: D3.js
 * Notes: Currently only supports donut and radar charts.
 */

var Charts = {
  radar: Radar,
  donut: Donut
};

exports.Charts = Charts;
})(window, document, undefined)