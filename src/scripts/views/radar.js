function Radar(data, options) {
  // Data
  this.data = data || [];

  // Options
  var o = options || {};
  var defaultOptions = {
    // SIZE - note that you will need to add a padding in CSS to make the labels visible
    // Current default padding in radar css is 80px.
    w: 600,
    h: 600,

    // OFFSET - useful to make the chart more readable by resizing/repositioning elements
    offsetFactor: 1,
    areaOffset: 0.95,
    legendaMaxOffset: 1,
    legendaOffset: 1.2,

    // Other options
    levels: 5, // concentric circles or polygons
    maxValue: 0, // leave to 0, if you want to control max value from data
    radians: 2 * Math.PI, // 360 deg = circle
    colors: [] // array of colors, corresponding to each series of data
  };
  this.options = extend(defaultOptions, o);

  this.initialize();
  this.render();
  // this.animate();
}

Radar.prototype = {

  initialize: function () {

  },

  render: function () {
    // console.log(this.data, this.options);

    var that = this;
    var d = this.data;
    var cfg = this.options;
    var id = this.options.el;

    // Get max value
    cfg.maxValue = Math.max(cfg.maxValue, d3.max(d, function (i) {
      return d3.max(i.map(function (o) {
        return o.value;
      }))
    }));

    // Get axis and label for each axis
    var allAxis = (d[0].map(function (i, j) {
      return i.label;
    }));

    // Useful values
    this.axisCount = allAxis.length;
    this.center = Math.min(cfg.w / 2, cfg.h / 2);
    this.radius = cfg.offsetFactor * Math.min(cfg.w / 2, cfg.h / 2);

    // Add main css classes
    this.$el = d3.select(id)
      .attr("class", "chart radar-chart");

    // Add wrapper, labels wrapper and svg
    this.$wrapper = this.$el
      .append("div")
      .attr("class", "wrapper")
      .style("width", cfg.w + "px")
      .style("height", cfg.h + "px")
      .style("position", "relative");
    this.$labelsWrapper = this.$wrapper
      .append("div")
      .attr("class", "labels-wrapper")
      .style("width", "100%")
      .style("height", "100%")
      .style("position", "absolute");
    this.$svg = this.$wrapper
      .append("svg")
      .attr("class", "svg")
      .attr("width", cfg.w)
      .attr("height", cfg.h);
    var g = this.$svg.append("g");

    // Position helpers
    function getPosition(i, range, offsetFactor, func) {
      offsetFactor = typeof offsetFactor !== 'undefined' ? offsetFactor : 1;
      return range * (1 - offsetFactor * func(i * cfg.radians / that.axisCount));
    }

    function getHorizontalPosition(i, range, offsetFactor) {
      return getPosition(i, range, offsetFactor, Math.sin);
    }

    function getVerticalPosition(i, range, offsetFactor) {
      return getPosition(i, range, offsetFactor, Math.cos);
    }

    // Draw concentric circles or polygons (svg)
    for (var i = 0; i < cfg.levels; i++) {
      var levelRadius = this.radius * ((i + 1) / cfg.levels);
      this.$levels = g
        .append("svg:circle")
        .attr('r', levelRadius)
        .attr("cx", this.center)
        .attr("cy", this.center)
        .style("opacity", 0)
        .attr("class", "level line");

    }

    // Draw axis lines (svg)
    var axis = g.selectAll(".axis")
      .data(d[0])
      .enter()
      .append("line")
      .style("opacity", 0)
      .attr("x1", cfg.w / 2)
      .attr("y1", cfg.h / 2)
      .attr("x2", function (j, i) {
        return getHorizontalPosition(i, cfg.w / 2, cfg.offsetFactor);
      })
      .attr("y2", function (j, i) {
        return getVerticalPosition(i, cfg.h / 2, cfg.offsetFactor);
      })
      .attr("class", "line axis");

    // Draw polygon area (svg)
    d.forEach(function (dataset, idx) {

      // Adapt data to draw the polygon area
      that.startDataValues = [];
      for (var i = 0; i < this.axisCount; i++) {
        that.startDataValues.push([
          getHorizontalPosition(i, cfg.w / 2, cfg.legendaMaxOffset),
          getVerticalPosition(i, cfg.h / 2, cfg.legendaMaxOffset)
        ]);
      }
      that.startDataValues.push(that.startDataValues[0]);

      that.endDataValues = [];
      g.selectAll(".nodes")
        .data(dataset, function (j, i) {
          that.endDataValues.push([
            getHorizontalPosition(i, cfg.w / 2, (parseFloat(Math.max(j.value, 0)) / cfg.maxValue) * cfg.areaOffset),
            getVerticalPosition(i, cfg.h / 2, (parseFloat(Math.max(j.value, 0)) / cfg.maxValue) * cfg.areaOffset)
          ]);
        });
      that.endDataValues.push(that.endDataValues[0]);

      // Draw polygon area
      g.selectAll(".area")
        .data([that.startDataValues])
        .enter()
        .append("polygon")
        .style("stroke", cfg.colors[idx])
        .style("opacity", 0)
        .attr("class", "polygon radar-chart-serie-" + idx)
        .attr("points", function (d) {
          var str = "";
          for (var i = 0; i < d.length; i++) {
            str = str + d[i][0] + "," + d[i][1] + " ";
          }
          return str;
        });

      // Draw legenda (dom)
      this.$labelsWrapper.selectAll(".legenda")
        .data(d[0])
        .enter()
        .append("div")
        .attr("class", "legenda")
        .style("opacity", 0)
        .text(function (d) {
          return d.label;
        })
        .style("color", function (d) {
          return d.color;
        })
        .style("top", function (d, i) {
          var y = getVerticalPosition(i, cfg.h / 2, cfg.legendaOffset);
          return y + "px";
        })
        .style("left", function (d, i) {
          var x = getHorizontalPosition(i, cfg.w / 2, cfg.legendaOffset);
          return x + "px";
        });

      // Draw max value dots (dom)
      this.$labelsWrapper.selectAll(".legenda-dot")
        .data(d[0])
        .enter()
        .append("div")
        .style("opacity", 0)
        .attr("class", "legenda-dot")
        .style("background-color", function (d) {
          return d.color;
        })
        .style("top", function (d, i) {
          var y = getVerticalPosition(i, cfg.h / 2, cfg.legendaMaxOffset);
          return y + "px";
        })
        .style("left", function (d, i) {
          var x = getHorizontalPosition(i, cfg.w / 2, cfg.legendaMaxOffset);
          return x + "px";
        })
        .append("div")
        .style("opacity", 0)
        .attr("class", "value")
        .text(function (d) {
          return d.max;
        });

      // Draw dots with values(dom)
      this.$labelsWrapper.selectAll(".nodes")
        .data(dataset)
        .enter()
        .append("div")
        .style("opacity", 0)
        .style("background-color", cfg.colors[idx])
        .attr("class", "circle radar-chart-serie-" + idx)
        .style("left", function (j, i) {
          var x = getHorizontalPosition(i, cfg.w / 2, (Math.max(j.value, 0) / cfg.maxValue) * cfg.areaOffset);
          return x + "px";
        })
        .style("top", function (j, i) {
          var y = getVerticalPosition(i, cfg.h / 2, (Math.max(j.value, 0) / cfg.maxValue) * cfg.areaOffset);
          return y + "px";
        })
        // .style("transform", "scale(0.5, 0.5)")
        .append("div")
        .attr("class", "value")
        .style("opacity", 0)
        .text(function (j) {
          return Math.max(j.value, 0)
        });

    }.bind(this));
  },

  animate: function () {
    this.animateLevels(
      this.animateAxis.bind(this,
        this.animateValues.bind(this))
    );
  },

  animateLevels: function (callback) {
    var that = this;
    var delay = 100;
    var duration = 800;
    var t = d3.transition()
      .duration(duration)
      .ease(d3.easeCubicInOut);
    var totalDuration = that.options.levels * delay + duration;

    this.$el.selectAll(".level")
      .transition(t)
      .delay(function (d, i) {
        return i * delay;
      })
      .attrTween("r", function (d, i) {
        var levelRadius = that.radius * ((i + 1) / that.options.levels);
        return d3.interpolate(0, levelRadius);
      })
      .style("opacity", 1);

    // Callback
    setTimeout(callback, totalDuration);
  },

  animateAxis: function (callback) {
    var that = this;
    var delay = 100;
    var duration = 500;
    var totalDuration = this.axisCount * delay + duration;

    var t = d3.transition()
      .duration(duration)
      .ease(d3.easeCubicInOut);

    // Lines
    this.$el.selectAll(".axis")
      .transition(t)
      .delay(function (d, i) {
        return i * delay;
      })
      .style("opacity", 1);

    // Max values
    this.$labelsWrapper
      .selectAll(".legenda-dot")
      .transition(t)
      .delay(function (d, i) {
        return i * delay;
      })
      .style("opacity", 1);

    // Legenda
    this.$labelsWrapper
      .selectAll(".legenda")
      .transition(t)
      .delay(function (d, i) {
        return i * (delay * 1.5);
      })
      .style("opacity", 1);

    // Area - start
    this.$svg.selectAll(".polygon")
      .transition(t)
      .delay(function (d, i) {
        return delay * that.axisCount;
      })
      .style("opacity", 0.8);

    // Callback
    setTimeout(callback, totalDuration);
  },

  animateValues: function (callback) {
    var that = this;
    var delay = 100;
    var duration = 1000;
    var totalDuration = duration / 2 + (that.axisCount * delay);

    var tArea = d3.transition()
      .duration(duration)
      .ease(d3.easeCubicInOut);
    var tValues = d3.transition()
      .duration(duration / 4)
      .ease(d3.easeCubicInOut);

    // Area
    this.$svg.selectAll(".polygon")
      .transition(tArea)
      .style("opacity", 1)
      .attr("points", function (d) {
        var endData = that.endDataValues;
        var str = "";
        for (var i = 0; i < d.length; i++) {
          str = str + endData[i][0] + "," + endData[i][1] + " ";
        }
        return str;
      });

    // Values
    setTimeout(function () {
      // Shot circles
      that.$labelsWrapper
        .selectAll(".circle")
        .transition(tValues)
        .delay(function (d, i) {
          return i * (delay);
        })
        .style("opacity", 1);

      that.$labelsWrapper
        .selectAll(".legenda-dot")
        .attr("class", "legenda-dot active");

      // Show labels
      setTimeout(function() {
        that.$labelsWrapper
          .selectAll(".circle")
          .attr("class", "circle active");
      }, totalDuration);

    }, duration);

    // Callback
    setTimeout(callback, totalDuration);
  }

};
