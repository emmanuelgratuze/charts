/**
 * Extend an object, by cloning its properties (like jQuery extend)
 * @param out
 * @returns {*|{}}
 */
var extend = function (out) {
  out = out || {};

  for (var i = 1; i < arguments.length; i++) {
    var obj = arguments[i];

    if (!obj)
      continue;

    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (typeof obj[key] === 'object')
          out[key] = extend(out[key], obj[key]);
        else
          out[key] = obj[key];
      }
    }
  }

  return out;
};


/**
 * D3 hack to get a callback after transitioning multiple elements.
 * Use: d3.selectAll("g").transition().call(endall, function() { console.log("done") });
 * @param transition
 * @param callback
 */
function endall(transition, callback) {
  if (!callback) callback = function(){};
  if (transition.size() === 0) { callback() }
  var n = 0;
  transition
    .each(function() { ++n; })
    .each("end", function() { if (!--n) callback.apply(this, arguments); });
}

/**
 * IsDOMElement - Check if variable is DOM Element
 * @param element
 * @returns {true|false}
 */
function isDOMElement(element) {
  return (typeof element === 'object') && (element.nodeType===1) && (typeof element.style === 'object') && (typeof element.ownerDocument === 'object')
}

/**
 * wrapSVGText - Wrap long labels
 * @param  {DOM Element} text
 * @param  {Number} width
 */
function wrapSVGText(element, width) {
  return element.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        x = text.attr("x"),
        dy = Math.abs(parseFloat(text.attr("dy"))),
        tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy",dy);

    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

/**
 * Clone object
 * @param  {Object} obj - Object to clone
 * @return {Object} Object cloned
 */
function cloneObject(obj) {
  if (obj === null || typeof(obj) !== 'object' || 'isActiveClone' in obj)
    return obj;

  if (obj instanceof Date)
    var temp = new obj.constructor(); //or new Date(obj);
  else
    var temp = obj.constructor();

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      obj['isActiveClone'] = null;
      temp[key] = cloneObject(obj[key]);
      delete obj['isActiveClone'];
    }
  }

  return temp;
}

/*
  Get max and min value in array
 */
Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

Array.prototype.min = function() {
  return Math.min.apply(null, this);
};